package com.sistemas.pollcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PollCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PollCoreApplication.class, args);
	}

}
