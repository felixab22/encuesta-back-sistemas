package com.sistemas.pollcore.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ENCUESTA")
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDENCUESTA")
    private Long idencuesta;

    @Column(name = "DENOMINACION")
    private String denominacion;

    @Column(name = "FINICIO")
    private String finicio;

    @Column(name = "FFIN")
    private String ffin;

    @Column(name = "FCREACION")
    private  String fcreacion;
}
